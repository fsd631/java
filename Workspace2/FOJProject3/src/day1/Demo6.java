package day1;

public class Demo6 {

	public static void main(String[] args) {
		// find even and odd indexes
		int arr[] = {10, 20, 30, 40, 50};
		int arrlength = arr.length;

		for(int i = 0; i < arrlength; i+=2){//odd index values
			System.out.print(arr[i] + " ");
		}
		System.out.println("\n");

		for(int i = 1; i < arrlength; i+=2){//even index values
			System.out.print(arr[i] + " ");
		}

	}

}
