package day1;

public class Demo5 {

	public static void main(String[] args) {
		// reverse of an array
		
				int arr[] = {10, 20, 30, 40, 50};
				int arrlength = arr.length;
				
				for (int i = 0; i < arrlength; i++){
					System.out.print(arr[i] + " ");
				}
				System.out.println("\n");
				

				for (int i = arr.length-1; i >= 0; i--){
					System.out.print(arr[i] + " ");
				}
	}

}
