package day1;

public class Demo1 {

	public static void main(String[] args) {
		// display array elements
				int arr[] = new int[5];

				arr[0] = 10;
				arr[1] = 20;
				arr[2] = 30;
				arr[3] = 40;
				arr[4] = 50;

				System.out.println("a[0] = " + arr[0]);
				System.out.println("a[1] = " + arr[1]);
				System.out.println("a[2] = " + arr[2]);
				System.out.println("a[3] = " + arr[3]);
				System.out.println("a[4] = " + arr[4]);

	}

}
