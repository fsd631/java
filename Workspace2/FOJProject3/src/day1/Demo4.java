package day1;

public class Demo4 {
	
	public static void main(String[] args) {
		// find min and max of an array
		int arr[] = {10, 20, 30, 40, 50};		
		int arrlength = arr.length;
		int min =arr[0];
		int max = arr[0];
		
		for(int i = 0; i<arrlength; i++){
			
			if (min > arr[i]){
				min = arr[i];
			}
			if (max < arr[i]){
				max = arr[i];
			}
		}
		
		System.out.println("min = " + min);
		System.out.println("max = " + max);

	}

}
