package day1;

public class Demo3 {

	public static void main(String[] args) {
		// sum and product of an array
		int arr[] = {10, 20, 30, 40, 50};		
		int arrlength = arr.length;
		int sum = 0;
		int product = 1;
		
		for(int i = 0; i<arrlength; i++){
			sum += arr[i];
			product *= arr[i];
		}
		
		System.out.println("sum =" +sum);
		System.out.println("product =" +product);

	}

}
