package day2;

public class Demo5 {
	public static int ternary2(int num1, int num2){
		return (num1 > num2) ? num1 : num2;
	}
	
	public static int ternary3(int num1, int num2, int num3){
		return (num1 > num2) ? 
				(num1 > num3) ? num1 : num3 :
					(num2 > num3) ? num2 :num3;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(ternary2(2, 3)); //3
		System.out.println(ternary3(3, 4, 9)); //9
		
	}

}
