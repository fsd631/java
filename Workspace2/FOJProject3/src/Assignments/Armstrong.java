package Assignments;

public class Armstrong {
	public static int power(int num, int p) {
        int result = 1;
        for (int i = 0; i < p; i++) {
            result *= num;
        }
        return result;
    }

    // Method to count the number of digits in a number
    public static int countDigits(int num) {
        int count = 0;
        while (num != 0) {
            num /= 10;
            count++;
        }
        return count;
    }
	 public static boolean isArmstrongNumber(int num) {
	        int sum = 0;
	        int originalNum = num;
	        int n = countDigits(num);

	        while (num != 0) {
	            int digit = num % 10;
	            sum += power(digit, n);
	            num /= 10;
	        }

	        return sum == originalNum;
	    }

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 int number = 153; // Change the number to check here

	        if (isArmstrongNumber(number)) {
	            System.out.println(number + " is an Armstrong number.");
	        } else {
	            System.out.println(number + " is not an Armstrong number.");
	        }

	}

}
