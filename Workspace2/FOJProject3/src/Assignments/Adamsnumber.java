package Assignments;

public class Adamsnumber {
	 public static int reverse(int num) {
	        int rev = 0;
	        while (num != 0) {
	            int digit = num % 10;
	            rev = rev * 10 + digit;
	            num /= 10;
	        }
	        return rev;
	    }
	 public static boolean isAdamNumber(int num) {
	        int square = num * num;
	        int reverseSquare = reverse(square);
	        int reverseNum = reverse(num);
	        int squareOfReverseNum = reverseNum * reverseNum;
	        return squareOfReverseNum == reverseSquare;
	    }

	public static void main(String[] args) {
		// Adams number
		  int number = 11; 

	        if (isAdamNumber(number)) {
	            System.out.println(number + " is an Adam's number.");
	        } else {
	            System.out.println(number + " is not an Adam's number.");
	        }

	}

}
