package Assignments;

import java.util.ArrayList;

public class CollatzSeq {

    // Method to generate Collatz sequence
    public static ArrayList<Integer> generateCollatzSequence(int num) {
        ArrayList<Integer> sequence = new ArrayList<>();
        sequence.add(num);

        while (num != 1) {
            if (num % 2 == 0) {
                num = num / 2;
            } else {
                num = 3 * num + 1;
            }
            sequence.add(num);
        }

        return sequence;
    }

    public static void main(String[] args) {
        int number = 20; 

        ArrayList<Integer> collatzSequence = generateCollatzSequence(number);

        System.out.println("Collatz sequence for " + number + ": " + collatzSequence);
    }
}
