package Assignments;

public class EvenOddSum {
	public static void evenoddsum(int arr[]){
		int evensum = 0;
		int oddsum = 0;
		
		for (int i = 0; i < arr.length; i++){
			if (arr[i] % 2 == 0){
				evensum += arr[i];
			}
			else{
				oddsum += arr[i] ;
			}
		}
		System.out.println("evensum: " +evensum);
		System.out.println("oddsum: " +oddsum);
		
		
	}
	
	public static void main(String[] args) {
		// Even odd sum
		int arr[] = {1, 2, 3, 4 , 5, 6, 7, 8, 9, 10};
		
		evenoddsum(arr);

	}

}
