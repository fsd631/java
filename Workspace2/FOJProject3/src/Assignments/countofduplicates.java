package Assignments;

public class countofduplicates {
    public static void countofArray(int arr[]) {
        int arrlength = arr.length;

        for (int i = 0; i < arrlength; i++) {
            int currentEle = arr[i];
            int count = 0; 
            boolean alreadyCounted = false;
            for (int j = 0; j < i; j++) {
                if (currentEle == arr[j]) {
                    alreadyCounted = true;
                    break; 
                }
            }
            if (!alreadyCounted) {
                for (int j = i; j < arrlength; j++) {
                    if (currentEle == arr[j]) {
                        count++;
                    }
                }         
                System.out.println("Count of " + currentEle + ": " + count);
            }
        }
    }

    public static void main(String[] args) {
        // Count the array elements
        int arr[] = {1, 2, 3, 1, 2, 3, 4};
        countofArray(arr);
    }
}
