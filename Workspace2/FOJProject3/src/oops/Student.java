package oops;

public class Student {
	int id;
	String name;
	double salary;
	
	public Student(int id,String name, double salary){
		this.id = id;
		this.name = name;
		this.salary = salary;
	}
	public void details(){
	System.out.println("Employee ID : " + id);	
	System.out.println("Employee Name : " + name);	
	System.out.println("salary : " + salary);
	}
	

	public static void main(String[] args) {
		Student student = new Student(21,"Susruthi Kanaparthi", 500000.00);
		student.details();
	}

}