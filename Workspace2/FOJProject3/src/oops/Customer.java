package oops;

public class Customer {
	private int id;
	private String firstName;
	private String lastName;
	private String address;
	private double balance;
	
	public Customer(){
		
	}
	public Customer(int id,String firstName, String lastName, String address, double balance){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.address = address;
		this.balance = balance;
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Customer c1 = new Customer(1001, "sachin", "tendulkar","20-20,EAST,MUMBAI",232342.93);
		System.out.println(c1);
		
		Customer c2 = new Customer(1002, "MS", "dhoni","20-20,WEST,MUMBAI",652242.93);
		System.out.println(c2);

	}

}
