package oops;

public class Circle {

	int Radius ;
	
	public void areaOfCircle(){
		double areaOfCircle = Math.PI * Radius * Radius ;
		System.out.println("The area of the Circle: " + areaOfCircle);
	}
	public static void main(String[] args) {
		Circle c = new Circle();
		c.Radius = 2;
		c.areaOfCircle();
	}

}
