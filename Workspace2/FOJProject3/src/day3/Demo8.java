package day3;

public class Demo8 {
	public static int secondlargest(int arr[]){
		 int largest = Integer.MIN_VALUE;
	        int secondLargest = Integer.MIN_VALUE;

	        for (int i = 0; i < arr.length; i++) {
	            if (arr[i] > largest) {
	                secondLargest = largest;
	                largest = arr[i];
	            } else if (arr[i] > secondLargest && arr[i] != largest) {
	                secondLargest = arr[i];
	            }
	        }

	        return secondLargest;
	}
	public static void main(String[] args) {
		// find second largest without sorting
		int arr1[] = {10, 30, 20, 40, 110};//40
		int arr2[] = {83, 96, 125, 183, 458, 320};
		int arr3[] = {83, 96};
		
		System.out.println(secondlargest(arr1));
		System.out.println(secondlargest(arr2));
		System.out.println(secondlargest(arr3));
		

	}

}
