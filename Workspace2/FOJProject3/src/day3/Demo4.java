package day3;

public class Demo4 {
	public static void rowsum(int arr[][]){
		int sum;
		
		for(int i =0; i < 3; i++){
			sum = 0;
			for(int j = 0; j < 3; j++){
				sum += arr[i][j];
			}
			System.out.print("row-" +(i + 1) + "sum = " +sum);
		}
		System.out.println("\n");
		
	}
	public static void main(String[] args) {
		// sum of rows in array
		int arr1[][] = new int[][]
				{{1, 0, 0}, {0, 1, 0}, {0, 0, 1} };

		int arr2[][] = new int[][]
				{{1, 2, 3}, {4, 5, 6}, {7, 8, 9} };

		int arr3[][] = new int[][]
				{{1, 0, 0}, {0, 1, 0}, {0, 0, 1} };

      rowsum(arr1);
      rowsum(arr2);
      rowsum(arr3);

	}

}
