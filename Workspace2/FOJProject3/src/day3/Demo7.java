package day3;

public class Demo7 {
    public static int[][] matrixMultiplication(int[][] matrix1, int[][] matrix2) {
        int m = matrix1.length;
        int n = matrix1[0].length;
        int p = matrix2[0].length;

        int[][] result = new int[m][p];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < p; j++) {
                int sum = 0;
                for (int k = 0; k < n; k++) {
                    sum += matrix1[i][k] * matrix2[k][j];
                }
                result[i][j] = sum;
            }
        }
        return result;
    }

    public static void main(String[] args) {
        int[][] matrix1 = {{0, 2, 3}, {4, 5, 6}, {7, 8, 9}};
        int[][] matrix2 = {{0, 0, 0}, {4, 5, 6}, {7, 8, 9}};

        int[][] result = matrixMultiplication(matrix1, matrix2);

        // Print the result
        System.out.println("Resultant Matrix:");
        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                System.out.print(result[i][j] + " ");
            }
            System.out.println();
        }
    }
}
