package day3;

public class Demo5 {
	public static int diagonalSum(int arr[][]){
		int sum = 0;

		for(int i = 0; i < 3; i++){
			for(int j = 0; j < 3; j++){
				if(i == j){
					sum += arr[i][j];
				}
			}
		}
		return sum;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int arr1[][] = new int[][]
				{{10, 20, 30}, {40, 50, 60}, {70, 80, 90} };

		int arr2[][] = new int[][]
				{{11, 21, 31}, {41, 51, 61}, {71, 81, 91} };

		int arr3[][] = new int[][]
				{{12, 22, 32}, {42, 52, 62}, {72, 82, 92} };

		System.out.println(diagonalSum(arr1));
		System.out.println(diagonalSum(arr2));
		System.out.println(diagonalSum(arr3));



	}

}