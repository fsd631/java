package day03;

//sum of two digits
public class Demo1 {
	public static int sum(int num){	
		return (num / 10 + num % 10);
	}

	public static void main(String[] args) {		
		System.out.println("sum = " + sum(23));
		System.out.println("sum = " + sum(93));
		System.out.println("sum = " + sum(28));
	}

}
