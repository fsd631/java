package day05;

public class Demo1 {

	public static void main(String[] args) {
		// loops-while
		int number = 5;
		int i = 1;
		
		while (i < 11){
			System.out.println(number + " * " + i + " = " +(number * i));
			i++;
		}

	}

}
