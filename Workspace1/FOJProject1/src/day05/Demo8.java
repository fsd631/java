package day05;

public class Demo8 {
	public static boolean isPrime(int num){
		if (num == 1){
			return false;
		}
		for (int i = 2 ; i< num; i++){
			if (num % i == 0){
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(isPrime(1));
		System.out.println(isPrime(121));
		System.out.println(isPrime(2121));

	}

}
