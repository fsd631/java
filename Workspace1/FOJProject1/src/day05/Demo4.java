package day05;
//calling the method itself is called recursion.
//sum of the numbers using recursion.
public class Demo4 {
	public static int sum(int num){
		if(num == 1){
			return 1;
		}
		
		return num + sum(num - 1);	
	}
	
	public static void main(String[] args) {
		// recursion
		System.out.println(sum(5));
		

	}

}
