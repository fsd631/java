package day05;

public class Demo2 {

	public static void main(String[] args) {
		// do-while
		int i = 1;
		
		do{
			System.out.println(i + " = " +(i * (i++)));			
		}while(i < 6);

	}

}
