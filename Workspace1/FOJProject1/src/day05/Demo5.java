package day05;

public class Demo5 {
	public static int factorial(int num){
		if(num == 1){
			return 1;
		}

		return num * factorial(num - 1);
	}
	
	public static void main(String[] args) {
		// factorial
		System.out.println(factorial(5));
	}

}
