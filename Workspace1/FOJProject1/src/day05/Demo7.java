package day05;

public class Demo7 {

	public static String generateFibSeries(int  num){

		int lastBefore = 0;
		int last = 1;
		int nextnum = 0;
		String result = "0 1 ";

		if(num < 0)
			return "-2";

		else if(num == 0)
			return "-1";

		else if(num == 1)
			return "0";

		else if(num == 2)
			return "1";

		else { 
			for (int i = 3; i <= num; i++){
				nextnum = lastBefore + last;
				lastBefore = last;
				last = nextnum;
				result +=nextnum + " ";
			}
		}
		return result;
	}

	public static void main(String[] args) {
		System.out.println(generateFibSeries(-1));
		System.out.println(generateFibSeries(1));
		System.out.println(generateFibSeries(5));
		System.out.println(generateFibSeries(10));
		System.out.println(generateFibSeries(0));

	}

}


