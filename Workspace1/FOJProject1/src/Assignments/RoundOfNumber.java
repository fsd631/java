package Assignments;
//Round of a number
public class RoundOfNumber {
	public static int round2(int num){
		int num1 = num;
		if ((num % 10) > 4){
			System.out.println("the round of the number " +num1+ " is:");
			return ((num / 10) + 1) * 10 ;
			
		}	
		
		else{
			if (num1 == 1){
				System.out.println("the round of the number " +num1+ " is:");
				return 0;
			}
			System.out.println("the round of the number " +num1+ " is:");
			return ((num1 / 10) - 1) * 10 ;
		}
	
	}

	public static void main(String[] args) {
		System.out.println(round2(1));
		System.out.println(round2(19));
		System.out.println(round2(25));
		System.out.println(round2(94));
		System.out.println(round2(99));
		
		
	}

}
