package Assignments;
//write a program for palindrome for 2 digit or digit number
//if input : 22 , 121 ,454 are palindrome numbers.
public class Palindrome23 {
	public static int palindrome2(int num){	
		//palindrome for 2-digit number
		if (num % 11 == 0){
			System.out.println("number " +num+ " is palindrome");
		}
		else{
			System.out.println("number " +num+ " is not palindrome");
		}
		
		return 0;
	}
	
	//palindrome for 3-digit number
	public static boolean palindrome3(int num){				
		return (num / 100) == (num % 10);
	}
	
	public static void main(String[] args) {
		System.out.println(palindrome2(22));
		System.out.println(palindrome2(23));
		System.out.println(palindrome3(122));
		System.out.println(palindrome3(121));

	}

}
