package Assignments;
//Name of the method isEven() // which accepts an integer value as argument and return 1 if the given number is even, else return 0.
//Arguments: one argument of type integer
//Return Type: an integer value 
//
//Specifications: The value returned by the method isEven() is determined by the following rules:
//
//if the given number is an even number, return 1 else return 0. Example if x = 22, return 1. if x = 35, return 0
//if the given number is negative or zero, return -1
public class evenodd {
	
	public static int isEven(int num){
		if (num % 2 == 0){
			return 0;
		}
		if (num % 2 == 1){
			return 1;
		}
		
		if (num < 0){
			return -1;
		}
		
		return 0;
	}

	public static void main(String[] args) {
		System.out.println(isEven(12));
		System.out.println(isEven(1));
		System.out.println(isEven(-13));
		

	}

}
