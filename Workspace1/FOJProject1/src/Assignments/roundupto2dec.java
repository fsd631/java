package Assignments;

// Convert 12.3333333 to 12.33
public class roundupto2dec {
    public static void round2(double value) {
        double roundedvalue = value * 100;
        roundedvalue = (int) roundedvalue;
        double result = roundedvalue / 100;

        System.out.println("The rounded value for the 2 decimal values is: " + result);
    }

    public static void main(String[] args) {
        round2(12.3333333);
    }
}

