package Assignments;
import java.util.Scanner;

public class meterstomilesandkm {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		 System.out.print("Enter distance in meters: ");
	        double distanceInMeters = scanner.nextDouble();
	        double distanceInMiles = distanceInMeters / 1609.0;

	        System.out.println(distanceInMeters + " meters is equal to " + distanceInMiles + " miles.");

	}

}
