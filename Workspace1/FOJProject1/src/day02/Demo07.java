package day02;

public class Demo07 {

	public static void main(String[] args) {
		// swapping numbers
		
		int num1 = 10;
		int num2 = 20;
		int temp;
		
		System.out.println("before swapping:");
		System.out.println("num1=" +num1 +"\nnum2=" +num2);
		
		
		
		System.out.println("after swapping with temp:");
		temp = num1;
		num1 = num2;
		num2 = temp;
		System.out.println("num1=" +num1 +"\nnum2=" +num2);
		
		
		
		int number1 = 10;
		int number2 = 20;
		
		System.out.println("after swapping with arithematic operations");
		number1 += number2;
		number2 = number1 - number2;
		number1 -= number2;
		System.out.println("num1=" +number1 +"\nnum2=" +number2);
		
		
		
		
		
		

	}

}
