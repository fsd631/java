package day02;

public class Demo3 {

	public static void main(String[] args) {
		// unary operators
		int num = 10;		
		System.out.println("num = " + num + "\n");
		
		num++;
		System.out.println("num = " + num + "\n");
		
		num--;
		System.out.println("num = " + num + "\n");
		

	}

}
