package day02;

public class Demo04 {

	public static void main(String[] args) {
		// preincreemnet or decreement operators
		
		int num1 = 10;
		int num2 = 0;
		System.out.println("before operations:");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
		
		//pre-increment
		num2 = ++ num1;
		System.out.println("applying pre-increment");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
		
		//post increment
		num2 = num1 ++;
		System.out.println("applying post-increment");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
		
		
		//pre-decrement
		num2 = -- num1;
		System.out.println("applying pre-decremnt");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
		
		//post increment
		num2 = num1 --;
		System.out.println("applying post-decremnt");
		System.out.println("num1=" + num1 + "\nnum2=" + num2);
				
				
		

	}

}
