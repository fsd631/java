package day02;

public class Demo6 {

	public static void main(String[] args) {
		// Type casting
		String s = "25";
		byte b = Byte.parseByte(s); //string to Byte
		short sh = Short.parseShort(s); //string to short
		int  i = Integer.parseInt(s); //string to Int
		long l = Long.parseLong(s); //string to Long
		
		//Lower to Higher
		System.out.println("Lower to Higher:");
		System.out.println("String s:" +s);
		System.out.println("Byte b:" +b);
		System.out.println("Short sh:" +sh);
		System.out.println("int i:" +i);
		System.out.println("Long l:" +l);
		
				
		
		l = 45;
		i = (int) i;
		sh = (short) sh;
		b = (byte) b;
		s = b + "";
		
		
		//Higher to Lower
		System.out.println("Higher to Lower:");
		System.out.println("Long l:" +l);
		System.out.println("int i:" +i);
		System.out.println("Short sh:" +sh);
		System.out.println("Byte b:" +b);
		System.out.println("String s:" +s);
		

	}

}
